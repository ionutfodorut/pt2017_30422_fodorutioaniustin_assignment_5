import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Ioan on 24.05.2017.
 */
public class Controller {

    ProcessData processData;
    View view;

    public Controller(ProcessData processData, View view) {
        this.processData = processData;
        this.view = view;
        this.view.addTask1Lister(new Task1Listener());
        this.view.addTask2Lister(new Task2Listener());
        this.view.addTask3Lister(new Task3Listener());
        this.view.addTask4Lister(new Task4Listener());
        this.view.addTask5Lister(new Task5Listener());
    }

    public class Task1Listener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            processData.countDistinctDays();
        }
    }

    public class Task2Listener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            processData.generateActivityCountMap();
        }
    }

    public class Task3Listener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            processData.generateDailyActivityCountMap();
        }
    }

    public class Task4Listener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            processData.generateActivityDurationMap();
        }
    }

    public class Task5Listener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            processData.generateActivityList();
        }
    }
}
