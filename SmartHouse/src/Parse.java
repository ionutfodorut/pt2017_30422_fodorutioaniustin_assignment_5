import java.time.LocalDateTime;

import static java.lang.Integer.parseInt;

/**
 * Created by John on 13.03.2017.
 */
public class Parse {

    public static MonitoredData parseMonitoredData(String s) {

        MonitoredData monitoredData = new MonitoredData(null, null, "");
        String[] tokens = s.split("[\t]");

        String[] startDateTime = tokens[0].split("[ ]");
        String[] startDate = startDateTime[0].split("[-]");
        String[] startTime = startDateTime[1].split("[:]");

        String[] endDateTime = tokens[2].split("[ ]");
        String[] endDate = endDateTime[0].split("[-]");
        String[] endTime = endDateTime[1].split("[:]");

        monitoredData.setStartTime(LocalDateTime.of(parseInt(startDate[0]), parseInt(startDate[1]), parseInt(startDate[2]),
                parseInt(startTime[0]), parseInt(startTime[1]), parseInt(startTime[2])));

        monitoredData.setEndTime(LocalDateTime.of(parseInt(endDate[0]), parseInt(endDate[1]), parseInt(endDate[2]),
                parseInt(endTime[0]), parseInt(endTime[1]), parseInt(endTime[2])));

        monitoredData.setActivityLabel(tokens[4]);

        return monitoredData;
    }

}

