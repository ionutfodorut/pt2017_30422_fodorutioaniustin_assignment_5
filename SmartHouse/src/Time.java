/**
 * Created by Ioan on 25.05.2017.
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Time {
    public static int getTime() {
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        Date dateobj = new Date();
        String time = df.format(dateobj);
        int timeInSecs = toSecs(time);
        return timeInSecs;
    }

    public static int toSecs(String s) {
        String[] hourMinSec = s.split(":");
        int hours = Integer.parseInt(hourMinSec[0]);
        int mins = Integer.parseInt(hourMinSec[1]);
        int secs = Integer.parseInt(hourMinSec[2]);
        return hours * 3600 + mins * 60 + secs;
    }

    public static String intToString (long i) {
        long hours = i / 3600;
        long mins = (i - hours*3600) / 60;
        long secs = i - hours*3600 - mins*60;
        return hours + ":" + mins + ":" + secs;
    }
}
