import java.io.*;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/** 
 * Created by Ioan on 24.05.2017.
 */
public class ProcessData {

    private List<MonitoredData> monitoredData;
    private String inputFile;

    public ProcessData(List<MonitoredData> monitoredData, String inputFile) {
        this.monitoredData = monitoredData;
        this.inputFile = inputFile;
    }

    public void readData() {
        try (BufferedReader br = new BufferedReader(new FileReader(inputFile))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                monitoredData.add(Parse.parseMonitoredData(sCurrentLine));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object writeToFile(Object o, String outputFile) {
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(outputFile))) {
            bw.append(String.format(o.toString()).concat("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return o;
    }

    public Integer countDistinctDays() {
        Integer n = monitoredData.stream()
                .collect(Collectors.groupingBy(a -> a.getStartTime().getDayOfYear()))
                .size();
        writeToFile(n, "Task1.txt");
        return n;
    }

    public Map<String, Long> generateActivityCountMap() {
        Map<String, Long> map = monitoredData.stream()
                .collect(Collectors.groupingBy(a -> a.getActivityLabel(), Collectors.counting()));
        writeToFile(map, "Task2.txt");
        return map;
    }

    public Map<Integer, Map<String, Long>> generateDailyActivityCountMap() {
        Map<Integer, Map<String, Long>> map = monitoredData.stream()
                .collect(Collectors.groupingBy(a -> a.getStartTime().getDayOfYear(), Collectors.groupingBy(b ->  b.getActivityLabel(), Collectors.counting())));
        writeToFile(map, "Task3.txt");
        return map;
    }

    public Map<String, Long> generateActivityDurationMap() {
        Map<String, Long> map =  monitoredData.stream()
                .collect(Collectors.groupingBy(a -> a.getActivityLabel(), Collectors.summingLong(p -> p.computeDuration())))
                .entrySet()
                .stream()
                .filter(p -> p.getValue() > 36000)
                .collect(Collectors.toMap(p->p.getKey(), p->p.getValue()));

        writeToFile(map, "Task4.txt");
        return map;
    }

    public List<String> generateActivityList() {
        Map<String, Long> map0 = monitoredData.stream()
                .filter(p -> p.getEndTime().toEpochSecond(ZoneOffset.UTC) - p.getStartTime().toEpochSecond(ZoneOffset.UTC) < 300)
                .collect(Collectors.groupingBy(p -> p.getActivityLabel(), Collectors.counting()));
        Map<String, Long> map1 = monitoredData.stream()
                .collect(Collectors.groupingBy(p -> p.getActivityLabel(), Collectors.counting()));
        List<String> list = map0.keySet().stream()
                .filter(p -> map1.containsKey(p) && (map0.get(p) / map1.get(p) > 0.9))
                .collect(Collectors.toList());
        writeToFile(list, "Task5.txt");
        return null;
    }

    public List<MonitoredData> getMonitoredData() {
        return monitoredData;
    }

    public void setMonitoredData(List<MonitoredData> monitoredData) {
        this.monitoredData = monitoredData;
    }

}
