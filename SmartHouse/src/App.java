import java.util.LinkedList;

/**
 * Created by Ioan on 25.05.2017.
 */
public class App {


    public static void main(String[] args) {

        ProcessData processData = new ProcessData(new LinkedList<MonitoredData>(), "Activities.txt");
        processData.readData();
        View view = new View();
        Controller controller = new Controller(processData, view);
        view.setVisible(true);

//        for (MonitoredData m: processData.getMonitoredData()) {
//            System.out.println(m.getEndTime().toString() + " "  + m.getStartTime().toString() + " " + m.getActivityLabel());
//        }

    }
}
